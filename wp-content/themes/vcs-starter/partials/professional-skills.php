<section class="lightest-main-color-background">
    <div class="row">
        <div class="same-size-paddings half-width full-professional-skills">
            <div class="small-letters">
                <h2 class="line-height-bigger"><?php the_field('p_s_heading'); ?></h2>
                <?php
                if(have_rows('p_s_skills_repeater')):
                    while (have_rows('p_s_skills_repeater')):
                        the_row();
                        ?>
                        <div>
                            <p class="dark-main-color uppercase bold"><?php the_sub_field('the_skill'); ?> <?php the_sub_field('percentage'); ?> %</p>
                            <div class="progress-bar full-width margin-top-smaller">
                                <div class="progress" style="width: <?php the_sub_field('percentage'); ?>%;"></div>
                            </div>
                        </div>
                    <?php
                    endwhile;
                endif;
                ?>
            </div>
        </div>
        <div class="half-width half-image">
            <?php $image = get_field('p_s_skills_image'); ?>
            <img src="<?php echo $image['sizes']['large'];?>" alt="<?php the_field('p_s_image_name');?>" class="full-width notebook">
        </div>
    </div>
</section>
