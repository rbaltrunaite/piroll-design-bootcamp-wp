<section class="lightest-main-color-background services">
    <div class="row flex-wrap" id="services">
        <?php
        if (have_rows('s_services_repeater')):
            while (have_rows('s_services_repeater')):
                the_row();
                ?>
                <div class="center fields similar-size-paddings">
                    <i class="<?php the_sub_field('service_icon'); ?> fa-3x main-color"></i>
                    <p class="dark-main-color uppercase bold small-letters margin-top"><?php the_sub_field('the_service'); ?></p>
                    <p class="main-color"><?php the_sub_field('description'); ?></p>
                </div>
            <?php
            endwhile;
        endif;
        ?>
    </div>
</section>
