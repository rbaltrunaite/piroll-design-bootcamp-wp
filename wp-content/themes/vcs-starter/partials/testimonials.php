<section class="accent-background">
    <div class="slider row center-content" id="testimonials">
        <div class="higher-size-paddings owl-carousel owl-theme center white">
            <?php
            if (have_rows('t_review_repeater')):
                while (have_rows('t_review_repeater')):
                    the_row();
                    ?>
                    <div class="margin-auto sliders similar-size-paddings">
                        <p class="quote bold">“ <?php the_sub_field('review'); ?> ”</p>
                        <p class="small-letters uppercase"><?php the_sub_field('reviewer'); ?></p>
                    </div>
                <?php
                endwhile;
            endif;
            ?>
        </div>
        <div class="owl-nav">
            <div class="owl-prev"></div>
            <div class="owl-next"></div>
        </div>
    </div>
</section>
