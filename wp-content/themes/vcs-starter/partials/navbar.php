<nav class="row navbar">
    <div>
        <a href="<?php bloginfo('url'); ?>">
            <?php $image = get_field('logo', 'option'); ?>
            <img src="<?php echo $image['sizes']['logo']; ?>" alt="<?php bloginfo('name'); ?>">
        </a>
    </div>
    <div class="small-letters">
        <?php $menu_settings = [
            'menu_class' => 'uppercase bold menu menu-different-screen-align',
            'container' => false,
            'theme_location' => 'primary-navigation'
        ];
        wp_nav_menu($menu_settings);
        ?>
    </div>
    <div class="menu-burger">
        <div></div>
        <div></div>
        <div></div>
    </div>
</nav>
