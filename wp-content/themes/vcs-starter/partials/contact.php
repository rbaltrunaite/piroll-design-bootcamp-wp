<section class="lightest-main-color-background contact">
    <div class="center same-size-paddings" id="contact">
        <h2 class="dark-main-color line-height-smaller"><?php the_field('c_heading'); ?></h2>
        <p class="text margin-auto main-color similar-size-paddings half-width"><?php the_field('c_description'); ?></p>
        <div id="#contact" class="flex-wrap row form">
            <div class="margin-auto">
            <?php echo do_shortcode(get_field('c_form_shortcode'));?>
            </div>
        </div>
    </div>
</section>
