<section class="similar-size-paddings">
    <div class="center" id="process">
        <h2 class="line-height-bigger dark-main-color"><?php the_field('w_p_heading'); ?></h2>
        <p class="main-color"><?php the_field('w_p_description'); ?></p>
    </div>
    <div class="center">
        <?php $image = get_field('w_p_video_poster');
            $link = get_field('w_p_video');
        ?>
        <video poster="<?php echo $image['sizes']['large']; ?>" controls class="top-bottom-margins">
            <source src="<?php echo $link['url']; ?>" type="video/mp4">
            <img src="<?php echo $image['sizes']['medium']; ?>" alt="thumbnail"/>
            Your browser does not support HTML5 video.
        </video>
    </div>
</section>
