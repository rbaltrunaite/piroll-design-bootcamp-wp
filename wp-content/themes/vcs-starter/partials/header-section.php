<?php
    $image = get_field('h_background_image');
?>

<header style="background-image: url(<?php echo $image['sizes']['large'] ?>);">
    <?php get_template_part('partials/navbar'); ?>
    <div class="row align-jumbotron">
        <div class="higher-size-paddings jumbotron">
            <h1 class="dark-main-color"><?php the_field('h_heading'); ?></h1>
            <p class="main-color margins"><?php the_field('h_description'); ?></p>
            <?php $button = get_field('h_link'); ?>
            <button class="small-letters uppercase white bold" id="contact-button"><?php echo $button['title']; ?></button>
        </div>
    </div>
</header>
