<section class="accent-background">
    <div class="higher-size-paddings">
        <div class="row flex-wrap main-color-higher-size-paddings">
            <?php
            if(have_rows('a_n_achievements_repeater')):
                while (have_rows('a_n_achievements_repeater')):
                    the_row();
                    ?>
                    <div class="white uppercase row">
                        <div><i class="<?php the_sub_field('icon');?> fa-3x"></i></div>
                        <div class="small-letters left-margin">
                            <h3><?php the_sub_field('number');?></h3>
                            <p><?php the_sub_field('achievement');?></p>
                        </div>
                    </div>
                <?php
                endwhile;
            endif;
            ?>
        </div>
    </div>
</section>
