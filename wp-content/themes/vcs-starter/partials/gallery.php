<section class="gallery">
    <?php
    $images = get_field('g_product_image');
    if( $images ): ?>
    <div class="container row flex-wrap">
        <?php foreach( $images as $image ): ?>
        <div class="product-div row">
            <a data-fancybox="products" href="<?php echo esc_url($image['url']); ?>" class="full-width">
                <img src="<?php echo esc_url($image['sizes']['gallery_image']); ?>" alt="<?php the_field('g_image_name'); ?>">
                <div class="overlay">
                    <div class="eye-image"></div>
                </div>
            </a>
        </div>
        <?php endforeach; ?>
    </div>
    <?php endif; ?>
    <div class="small-letters uppercase main-color center full-width bold lightest-main-color-background similar-size-paddings load-more-work bold"> Load more work</div>
</section>
