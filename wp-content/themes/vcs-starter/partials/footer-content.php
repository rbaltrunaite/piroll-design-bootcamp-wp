<footer class="dark-main-color-background">
    <div class="same-size-paddings footer-to-screen-size row space-between center">
        <div class="footer-upper-line">
            <h3 class="white"><?php the_field('f_name'); ?></h3>
            <p class="margin-top middle-color">	&copy; <?php the_field('f_designer'); ?></p>
        </div>
        <div class="footer-upper-line">
            <?php $email = get_field('f_email'); ?>
            <a href="mailto:<?php echo $email['url'] ;?>;?>"><?php echo $email['url'] ;?></a> <br>
            <?php $number = get_field('f_phone_number'); ?>
            <a href="tel:<?php echo $number['url'] ;?>"><?php echo $number['url'] ;?></a>
        </div>
        <div class="footer-lower-line">
            <ul>
                <?php
                if(have_rows('f_main_additional_links_repeater')):
                    while (have_rows('f_main_additional_links_repeater')):
                        the_row();
                        $link = get_sub_field('additional_link');
                        ?>
                        <li>
                            <a href="<?php echo $link['url'] ;?>" <?php echo $link['target'] ? 'target="_blank"' : ''; ?>>
                                <?php echo $link['title'] ;?>
                            </a>
                        </li>
                    <?php
                    endwhile;
                endif;
                ?>
            </ul>
        </div>
        <div class="footer-lower-line">
            <ul>
                <?php
                if(have_rows('f_additional_links_repeater')):
                    while (have_rows('f_additional_links_repeater')):
                        the_row();
                        $link = get_sub_field('additional_link');
                        ?>
                        <li>
                            <a href="<?php echo $link['url'] ;?>" <?php echo $link['target'] ? 'target="_blank"' : ''; ?>>
                                <?php echo $link['title'] ;?>
                            </a>
                        </li>
                    <?php
                    endwhile;
                endif;
                ?>
            </ul>
        </div>
        <div class="footer-lower-line">
            <ul>
                <?php
                if(have_rows('f_social_networks_repeater')):
                    while (have_rows('f_social_networks_repeater')):
                        the_row();
                        $link = get_sub_field('social_networks');
                        ?>
                        <li>
                            <a href="<?php echo $link['url'] ;?>" <?php echo $link['target'] ? 'target="_blank"' : ''; ?>>
                                <?php echo $link['title'] ;?>
                            </a>
                        </li>
                    <?php
                    endwhile;
                endif;
                ?>
            </ul>
        </div>
    </div>
</footer>
