<section>
    <div class="desktop-align">
        <div class="middle higher-size-paddings center" id="about">
            <h2 class="dark-main-color line-height-smaller"><?php the_field('a_heading'); ?></h2>
            <div class="middle">
                <div class="text main-color margins">
                    <p><?php the_field('a_description'); ?></p>
                </div>
            </div>
            <?php $image = get_field('a_signature_image'); ?>
            <img src="<?php echo $image['sizes']['signature'];?>" alt="<?php the_field('a_image_name');?>">
        </div>
    </div>
</section>
