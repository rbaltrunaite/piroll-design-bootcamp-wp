<section>
    <div class="higher-size-paddings row center parent-clients-fields">
        <?php
        if(have_rows('c_client_image_repeater')):
            while (have_rows('c_client_image_repeater')):
                the_row();
                $image = get_sub_field('image');
                ?>
                <div class="clients-fields">
                    <img src="<?php echo $image['sizes']['medium']; ?>" alt="<?php the_sub_field('clients_name');?> logo">
                </div>
            <?php
            endwhile;
        endif;
        ?>
    </div>
</section>
