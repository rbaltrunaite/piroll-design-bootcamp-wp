<?php

// Įjungiame post thumbnail

add_theme_support( 'post-thumbnails' );

// Apsibrėžiame stiliaus failus ir skriptus

if( !defined('THEME_FOLDER') ) {
	define('THEME_FOLDER', get_bloginfo('template_url'));
}

function theme_scripts(){

    if ( !is_admin() ) {
        /*<script src="assets/scripts/jquery.js"></script>
    <script src="assets/scripts/owl.carousel.min.js"></script>
    <script src="assets/scripts/jquery.fancybox.min.js"></script>
    <script src="assets/scripts/custom.js"></script>
        <script src="https://kit.fontawesome.com/dc8d931fc9.js" crossorigin="anonymous"></script>*/

    	//wp_register_script(handle, path, dependency, version, load_in_footer);

        wp_deregister_script('jquery');
		wp_register_script('jquery', THEME_FOLDER . '/assets/scripts/jquery.js', false, false, true);

    	//Registration
        wp_register_script('carousel', THEME_FOLDER . '/assets/scripts/owl.carousel.min.js', array('jquery'), false, true);
        wp_register_script('fontawesome', 'https://kit.fontawesome.com/dc8d931fc9.js', array('carousel'), false, true);
        wp_register_script('fancybox', THEME_FOLDER . '/assets/scripts/jquery.fancybox.min.js', array('fontawesome'), false, false);
        wp_register_script('custom', THEME_FOLDER . '/assets/scripts/custom.js', array('fancybox'), false, true);
        //Loading
        wp_enqueue_script('jquery');
        wp_enqueue_script('carousel');
        wp_enqueue_script('fontawesome');
        wp_enqueue_script('fancybox');
        wp_enqueue_script('custom');
    }
}
add_action('wp_enqueue_scripts', 'theme_scripts');

/*function custom_add_google_fonts() {
    wp_enqueue_style( 'custom-google-fonts', 'https://fonts.googleapis.com/css?family=Montserrat:500,700|Nunito+Sans:400,700|Work+Sans&display=swap', false );
}
add_action( 'wp_enqueue_scripts', 'custom_add_google_fonts' );*/

function theme_stylesheets(){

	if( defined('THEME_FOLDER') ) {
		//wp_register_style(handle, path, dependency, version, devices);	

		//Registration
        
        wp_register_style('fancybox', THEME_FOLDER . '/assets/css/jquery.fancybox.min.css', array(), false, 'all');
        wp_register_style('carousel', THEME_FOLDER . '/assets/css/owl.carousel.min.css', array('fancybox'), false, 'all');
        wp_register_style('theme', THEME_FOLDER . '/assets/css/owl.theme.default.min.css', array('carousel'), false, 'all');
        wp_register_style('style', THEME_FOLDER . '/assets/css/style.css', array('theme'), false, 'all');
        //Loading
        wp_enqueue_style('fancybox');
        wp_enqueue_style('carousel');
        wp_enqueue_style('theme');
        wp_enqueue_style('style');
        wp_enqueue_style( 'custom-google-fonts', 'https://fonts.googleapis.com/css?family=Montserrat:500,700|Nunito+Sans:400,700|Work+Sans&display=swap', false );

    }
}
add_action('wp_enqueue_scripts', 'theme_stylesheets');

// Apibrėžiame navigacijas

function register_theme_menus() {
   
	register_nav_menus(array( 
        'primary-navigation' => __( 'Primary Navigation' ) 
    ));
}

add_action( 'init', 'register_theme_menus' );

// Apibrėžiame widgets juostas

#$sidebars = array( 'Footer Widgets', 'Blog Widgets' );

if( isset($sidebars) && !empty($sidebars) ) {

	foreach( $sidebars as $sidebar ) {

		if( empty($sidebar) ) continue;

		$id = sanitize_title($sidebar);

		register_sidebar(array(
			'name' => $sidebar,
			'id' => $id,
			'description' => $sidebar,
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>'
		));
	}
}

if ( function_exists('acf_add_options_page')) {
    acf_add_options_page();
};

function dump($data) {
    echo  "<pre>";
    print_r($data);
    echo "</pre>";
}

add_image_size('logo', 38, 42, false);

add_image_size('signature', 225, 60, false);

add_image_size('gallery_image', 500, 500, true);
