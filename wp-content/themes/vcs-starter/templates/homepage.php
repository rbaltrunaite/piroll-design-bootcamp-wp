<?php

/*Template Name: Homepage*/

get_header();

get_template_part('partials/header-section');
get_template_part('partials/about');
get_template_part('partials/professional-skills');
get_template_part('partials/achievements-in-numbers');
get_template_part('partials/gallery');
get_template_part('partials/work-process');
get_template_part('partials/services');
get_template_part('partials/testimonials');
get_template_part('partials/clients');
get_template_part('partials/contact');

 get_footer(); ?>

