$(document).ready(function () {

    //Galerija
    $('[data-fancybox="products"]').fancybox({
        loop: true,
        animationDuration: 1000,
    });

    //Burgerio meniu
    $(".menu-burger").click(function () {
        $(".menu-different-screen-align").toggleClass("show");
    });

    //Testimonial dalis
    $('.owl-carousel').owlCarousel({
        loop: true,
        nav: true,
        autoplay: true,
        navText: ['<i class="fa fa-2x fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-2x fa-angle-right" aria-hidden="true"></i>'],
        autoplayTimeout: 4000,
        margin: 10,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    });

    //Parodyti daugiau / maziau produktu
    $(window).width();
    let width = $(window).width();

    if (width >= 750) {
        $(".load-more-work").click(function () {
            $(".product-div:nth-child(n+9)").slideToggle("slow");
        });
    } else {
        $(".load-more-work").click(function () {
            $(".product-div:nth-child(n+5):nth-child(n+9)").slideToggle("slow");
        });
    }

    (function () {
        let count = 0;
        $(".load-more-work").click(function () {
            count += 1;
            if (count % 2 === 0) {
                $(".load-more-work").text("Load more work");
            } else {
                $(".load-more-work").text("Show less");
            }
        });
    })();

    //Navigacija (kad veiktu leciau, animuotai)
    document.querySelectorAll('a[href^="#"]').forEach(anchor => {
        anchor.addEventListener('click', function (e) {
            e.preventDefault();

            document.querySelector(this.getAttribute('href')).scrollIntoView({
                behavior: 'smooth'
            });
        });
    });

    //Susisiekimo mygtuko animacija ir veikimas
    $("#contact-button").click(function() {
        $('html,body').animate({
                scrollTop: ($("form").offset().top - 220)},
            1600);
    });

});
