<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'bootcampwpmine' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '7IXMnhq$Y(PE$rf9Wmoy?j^(XPN8oSTE#*N^xem5>D.v&e(pKhW:UF__o.*DrNCR' );
define( 'SECURE_AUTH_KEY',  'rV7>FM`>q$M(WCSF,IA:@jr^XdeLyH!gA7t)Jf,vQ[lnJMV}4SML49/_3a`pcQz{' );
define( 'LOGGED_IN_KEY',    'G135^iR)@OF30#nmY-:jgv:>/@@+9pzsasGXI%x{htCip5VO1+jpL=]1#n-9N 1,' );
define( 'NONCE_KEY',        '[khOu8GZu1?%~J% &//8r%ck0yTc$wJ2li3{x]zLY/Ej]A^A/-b69Uyd=/YS44~q' );
define( 'AUTH_SALT',        'GrM!IH/>aRQ?#nG#R1}GN .w4c`J&S;l%`]W)lg;,BgcHN&;pzhbZk}L_,>O0/yd' );
define( 'SECURE_AUTH_SALT', 'D~/po=Ki(QnkdY2q~[(S$,-,T}:WV@.qENb0%{ivXRx{WOeXNDyxZ8[6Q^FBhO-]' );
define( 'LOGGED_IN_SALT',   'hm%rRh8^bXOEv^A#H/o0^a|[Y1$7NZrN:eLhgJ708R~?}Th Q^gNuf55I!01k8nF' );
define( 'NONCE_SALT',       '1CiC4GGSgBAv}%,~1N}?-smoVU|BLcq,0etS!ZJTf.(QGI9a+U/16[a]jevZ?j?y' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
